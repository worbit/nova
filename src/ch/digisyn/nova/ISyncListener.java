package ch.digisyn.nova;

public interface ISyncListener {
	void sync(int seqNum);
}
